import { Component } from '@angular/core';
import { CurrentWeather } from './Models/current.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'WeatherApp';

  currentWeatherData: CurrentWeather;

  onNewForecast(data){
    console.log('otrzymalem pogode');
    console.log(data);
    this.currentWeatherData = data;
  }
}

