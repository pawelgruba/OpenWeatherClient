import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ApiClientService } from '.././services/apiClient/api-client.service';
import { CurrentWeather } from '../Models/current.model';
import { ConfigService } from '../services/config.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  cityName:string = "Lubartów";
  @Output() currentWeatherEvent: EventEmitter<CurrentWeather> = new EventEmitter();

  constructor(private apiClient: ApiClientService) { }

  ngOnInit() {
  }

  showWeather(){
    console.log(this.cityName);
    let currentWeather = this.apiClient.getCurrentWeather(this.cityName);
    currentWeather.subscribe((data)=>{
      this.currentWeatherEvent.emit(data);
    })

  }

}
