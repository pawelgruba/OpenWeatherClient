import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { CurrentWeatherComponent } from './current-weather/current-weather.component';
import { HumanDateFilter } from './Filters/humanDateFilter';
import { WindComponent } from './controls/wind/wind.component';
import { TemperaturesComponent } from './controls/temperatures/temperatures.component';
import { TempFromKalvinPipe } from './Filters/temp-from-kalvin.pipe';
import { PressureHumidityComponent } from './controls/pressure-humidity/pressure-humidity.component';


@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    CurrentWeatherComponent,
    HumanDateFilter,
    WindComponent,
    TemperaturesComponent,
    TempFromKalvinPipe,
    PressureHumidityComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
