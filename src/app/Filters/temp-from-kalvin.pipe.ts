import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'tempFromKalvin'
})
export class TempFromKalvinPipe implements PipeTransform {

  transform(value: number): any {
    return value - 273.15;
  }

}
