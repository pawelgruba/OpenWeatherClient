import {  WeatherModel } from "./weather.model";
import { CoordinatesModel } from "./coordinates.model";
import { MainWeatherModel } from "./main-weather-model";
import { WindModel } from "./wind-model";
import { CloudsModel } from "./clouds-model";
import { SystemModel } from "./system-model";

export class CurrentWeather {
    public coord: CoordinatesModel;
    public weather: WeatherModel[];
    public base: string;
    public main: MainWeatherModel;
    public wind: WindModel;
    public clouds: CloudsModel;
    public dt: number;
    public sys: SystemModel;
    public id: number;
    public name: string;
    public cod: number;
}