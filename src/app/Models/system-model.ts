export class SystemModel {
    public message: number;
    public country: string;
    public sunrise: number;
    public sunset: number;
}

