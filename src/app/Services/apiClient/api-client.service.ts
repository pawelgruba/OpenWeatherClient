import { Injectable } from '@angular/core';
import { HttpClientModule }    from '@angular/common/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CurrentWeather } from '../../Models/current.model';
import { Observable } from '../../../../node_modules/rxjs';
import { ConfigService } from '../config.service';

@Injectable({
  providedIn: 'root',
})

export class ApiClientService {  

  constructor(private http: HttpClient, private appConfig: ConfigService) { }

  
  getCurrentWeather(city:string): Observable<CurrentWeather>{
    console.log("Getting weather for "+city);
    return this.http.get<CurrentWeather>(`http://api.openweathermap.org/data/2.5/weather?q=${city}&APPID=${this.appConfig.configuration.apiKey}`);
  }
}
