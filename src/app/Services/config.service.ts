import { Injectable } from '@angular/core';
import * as config from '../appConfig.json';
import { AppConfigModel } from '../Models/appSetting.model';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  public configuration: AppConfigModel;

  constructor() {
    this.configuration = config.default as AppConfigModel;
  }  
}
