import { Component, OnInit, Input } from '@angular/core';
import { CurrentWeather } from '../Models/current.model';

@Component({
  selector: 'app-current-weather',
  templateUrl: './current-weather.component.html',
  styleUrls: ['./current-weather.component.css']
})
export class CurrentWeatherComponent implements OnInit {

  constructor() { }

  @Input('currentWeatherModel') currentWeatherData: CurrentWeather;

  ngOnInit() {
  }

  

}
