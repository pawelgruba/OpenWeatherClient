import { Component, OnInit, Input } from '@angular/core';
import { MainWeatherModel } from '../../Models/main-weather-model';

@Component({
  selector: 'app-pressure-humidity',
  templateUrl: './pressure-humidity.component.html',
  styleUrls: ['./pressure-humidity.component.css']
})
export class PressureHumidityComponent implements OnInit {

  @Input() main:MainWeatherModel;

  constructor() { }

  ngOnInit() {
  }

}
