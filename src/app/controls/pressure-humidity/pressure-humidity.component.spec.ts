import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PressureHumidityComponent } from './pressure-humidity.component';

describe('PressureHumidityComponent', () => {
  let component: PressureHumidityComponent;
  let fixture: ComponentFixture<PressureHumidityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PressureHumidityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PressureHumidityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
