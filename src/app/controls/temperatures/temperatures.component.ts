import { Component, OnInit, Input } from '@angular/core';
import { WeatherModel } from '../../Models/weather.model';
import { MainWeatherModel } from '../../Models/main-weather-model';

@Component({
  selector: 'app-temperatures',
  templateUrl: './temperatures.component.html',
  styleUrls: ['./temperatures.component.css']
})
export class TemperaturesComponent implements OnInit {

  @Input() weather:WeatherModel;
  @Input() main:MainWeatherModel;

  constructor() { }

  ngOnInit() {
  }

  getWeatherIconAddress(){
    return `http://openweathermap.org/img/w/${this.weather.icon}.png`;
  }

}
