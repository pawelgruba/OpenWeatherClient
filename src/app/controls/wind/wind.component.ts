import { Component, OnInit, Input } from '@angular/core';
import { WindModel } from '../../Models/wind-model';

@Component({
  selector: 'app-wind',
  templateUrl: './wind.component.html',
  styleUrls: ['./wind.component.css']
})
export class WindComponent implements OnInit {

  @Input() wind:WindModel;
  constructor() { }

  ngOnInit() {
  }

  applyRotation(){
      return {'transform': 'rotate('+ (this.wind.deg) + 'deg)'}


  }

  

}
